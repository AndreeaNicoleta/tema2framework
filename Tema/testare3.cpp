#include <Windows.h>
#include <iostream>
#include "../../staticLibrary/staticLibrary/staticLibraryFunction.h"
//typedef void(_cdecl * staticLibraryFunctionProcedure)();
typedef void(_cdecl * firstDllFunctionProcedure)();
typedef void(_cdecl * secondDllFunctionProcedure)();

int main()
{
//	HINSTANCE hModule1 = LoadLibrary(TEXT("staticLibrary.lib"));
	HINSTANCE hModule2 = LoadLibrary(TEXT("firstDll.dll"));
	HINSTANCE hModule3 = LoadLibrary(TEXT("secondDll.dll"));

//	staticLibraryFunctionProcedure pstaticLibraryFunction = (staticLibraryFunctionProcedure)GetProcAddress(hModule1, "staticLibraryFunction");
	firstDllFunctionProcedure pfirstDllFunction = (firstDllFunctionProcedure)GetProcAddress(hModule2, "firstDllFunction");
	secondDllFunctionProcedure psecondDllFunction = (secondDllFunctionProcedure)GetProcAddress(hModule3, "secondDllFunction");

//	if (pstaticLibraryFunction)
	 //	pstaticLibraryFunction();
	//else
		//std::cout << "aiic";

	staticLibraryFunction();
		std::cout << std::endl;

	if (pfirstDllFunction)
		pfirstDllFunction();

	std::cout << std::endl;
	if (psecondDllFunction)
		psecondDllFunction();
	std::cout << std::endl;

//	FreeLibrary(hModule1);
	FreeLibrary(hModule2);
	FreeLibrary(hModule3);


}