#include <Windows.h>
#include <iostream>
#include "../../staticLibrary/staticLibrary/staticLibraryFunction.h"
//typedef void(_cdecl * staticLibraryFunctionProcedure)();
typedef void(_cdecl * firstDllFunctionProcedure)();
typedef void(_cdecl * secondDllFunctionProcedure)();
typedef void(_cdecl * SomeMethodProcedure)();
typedef void(_cdecl * SomeOtherMethodProcedure)();
typedef void(_cdecl * minvalProcedure)(int, int);
typedef void(_cdecl * copyStringsProcedure)();
typedef void(_cdecl * initialize_stringProcedure)();
typedef void(_cdecl * MyClassObjectProcedure)();

int main()
{
//	HINSTANCE hModule1 = LoadLibrary(TEXT("staticLibrary.lib"));
	HINSTANCE hModule2 = LoadLibrary(TEXT("firstDll.dll"));
	HINSTANCE hModule3 = LoadLibrary(TEXT("secondDll.dll"));

//	staticLibraryFunctionProcedure pstaticLibraryFunction = (staticLibraryFunctionProcedure)GetProcAddress(hModule1, "staticLibraryFunction");
	firstDllFunctionProcedure pfirstDllFunction = (firstDllFunctionProcedure)GetProcAddress(hModule2, "firstDllFunction");
	secondDllFunctionProcedure psecondDllFunction = (secondDllFunctionProcedure)GetProcAddress(hModule3, "secondDllFunction");
	SomeMethodProcedure pSomeMethod = (SomeMethodProcedure)GetProcAddress(hModule2, "SomeMethodFunction");
	SomeOtherMethodProcedure pSomeOtherMethod = (SomeOtherMethodProcedure)GetProcAddress(hModule2, "SomeOtherMethod");
	minvalProcedure pminval = (minvalProcedure)GetProcAddress(hModule2, "minval");
	copyStringsProcedure pcopyStrings = (copyStringsProcedure)GetProcAddress(hModule2, "copyStrings");
	initialize_stringProcedure pinitialize_string = (initialize_stringProcedure)GetProcAddress(hModule2, "initialize_string");
	MyClassObjectProcedure pMyClassObject = (MyClassObjectProcedure)GetProcAddress(hModule2, "MyClassObject");

//	if (pstaticLibraryFunction)
	 //	pstaticLibraryFunction();
	//else
		//std::cout << "aiic";

	staticLibraryFunction();
		std::cout << std::endl;

	if (pfirstDllFunction)
		pfirstDllFunction();

	std::cout << std::endl;
	if (psecondDllFunction)
		psecondDllFunction();
	std::cout << std::endl;

	if (pSomeMethod)
		pSomeMethod();
	std::cout << std::endl;

	int A[] = { 10, 20, 30 };
	pminval(*A, 3);

	if (pcopyStrings)
		pcopyStrings();
	std::cout << std::endl;

	if (pinitialize_string)
		pinitialize_string();
	std::cout << std::endl;

	if (pMyClassObject)
		pMyClassObject();
	std::cout << std::endl;

	

//	FreeLibrary(hModule1);
	FreeLibrary(hModule2);
	FreeLibrary(hModule3);


}