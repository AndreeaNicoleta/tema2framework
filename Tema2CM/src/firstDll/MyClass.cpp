#include "MyClass.h"
#include<iostream>


MyClass::MyClass()
{

}

//Allowing Exceptions to Leave Destructors
/*It is not frequently necessary to throw an exception from a destructor. Even then, there is a better way to do that. 
However, exceptions are mostly not thrown from destructors explicitly. It can happen that a simple command to log a 
destruction of an object causes an exception throwing.*/
MyClass::~MyClass()
{
	writeToLog(); // could cause an exception to be thrown
}

void MyClass::writeToLog()
{

	try
	{
		throw 'c';
	}
	catch (int e)
	{
		std:: cout << "An exception occurred. Exception Nr. " << e << '\n';
	}
}
