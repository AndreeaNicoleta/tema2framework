#include<iostream>
#include "firstDll.h"
#include <string.h>
#include "ClassA.h"

void firstDllFunction(void)
{
	std::cout << "Primul dll!";
}


// 1. Using "new" and "delete" Pairs Incorrectly
/*If an exception is thrown, the "a" object is never deleted. The following example shows a safer and shorter 
way to do that. It uses auto_ptr which is deprecated in C++, but the old standard is still widely used. It can be 
replaced with C++ unique_ptr or scoped_ptr from Boost if possible.*/
void SomeMethod()
{
	ClassA *a = new ClassA(10);
	SomeOtherMethod();      // it can throw an exception
	delete a;
}

void SomeOtherMethod()
{
	try
	{
		throw 10;
	}
	catch (int e)
	{
		std::cout << "An exception occurred. Exception Nr. " << e << '\n';
	}
}

// 2. Missing or improper initialization
//Since currmin was never initialized, it could easily start out as the minimum value. 
//Some compilers spot no-initialization errors. 
//Note that an improper initialization, while rarer, is even harder to spot than a missing one!
int minval(int *A, int n) {
	int currmin;

	for (int i = 0; i<n; i++)
		if (A[i] < currmin)
			currmin = A[i];
	return currmin;
}

//3. Parameter mismatch (instance of a model error)
/*Oops -- strcpy's first parameter is the destination, and its second parameter is the source, 
not the other way around! Whether this is a dyslexic error or a model error of course depends on what 
the programmer believes the proper order to be*/
void copyStrings()
{

	char string1[10] = "Hello";
	char string2[10];

	strcpy(string1, string2);
}

//4. Memory error
/*Oops! "hello" has a sixth character, the terminator. We just corrupted one of the surrounding variables on the stack.*/
void initialize_string()
{
	char string[5] = "hello";
	
	
}

//5. The follow bugs are in MyClass.cpp
//Allowing Exceptions to Leave Destructors

void MyClassObject()
{
	try
	{
		MyClass a1;
		MyClass a2;
	}
	catch (std::exception& e)
	{
		std::cout << "exception caught";
	}
}
/*In the code above, if exception occurs twice, such as during the destruction of both objects, the catch statement 
is never executed. Because there are two exceptions in parallel, no matter whether they are of the same type or 
different type the C++ runtime environment does not know how to handle it and calls a terminate function which results 
in termination of a programís execution.*/
