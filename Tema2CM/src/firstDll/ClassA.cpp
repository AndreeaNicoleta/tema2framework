#include "ClassA.h"



ClassA::ClassA(int nr)
{
	this->number= nr;
}


ClassA::~ClassA()
{
}

//5. Forgotten Virtual Destructor
/*This is one of the most common errors that leads to memory leaks inside derived classes if there is dynamic
memory allocated inside them.*/