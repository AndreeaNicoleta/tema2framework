#pragma once
#include "MyObject.h"
using namespace std;
class MyWidget :
	public MyObject
{
protected:
	int position;
	int cursor;
public:
	MyWidget(const MyWidget &obj);

	MyWidget(int position, int cursor);
	~MyWidget();
	string getName();
};

