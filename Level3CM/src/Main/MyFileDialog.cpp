#include "MyFileDialog.h"



MyFileDialog::MyFileDialog(bool preview) :MyDialog(visible)
{
	this->preview = preview;
}

MyFileDialog::MyFileDialog(const MyFileDialog &obj) : MyDialog(visible)
{
	preview = obj.preview;
}

MyFileDialog::~MyFileDialog()
{
	std::cout << "Destructor from MyFileDialog class.";
}

int MyFileDialog::sizeHint() {
	return 2;
}

string MyFileDialog::getName()
{
	return "MyFileDialog";
}
