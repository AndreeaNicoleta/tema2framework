#include "MyErrorMessage.h"



MyErrorMessage::MyErrorMessage(bool showOnlyOnce) :MyDialog(visible)
{
	this->showOnlyOnce = showOnlyOnce;
}

MyErrorMessage::MyErrorMessage(const MyErrorMessage &obj) : MyDialog(visible)
{
	showOnlyOnce = obj.showOnlyOnce;
}

MyErrorMessage::~MyErrorMessage()
{
	std::cout << "Destructor from MyErrorMessage class.";
}

int MyErrorMessage::sizeHint() {
	return 3;
}
string MyErrorMessage::getName()
{
	return "MyErrorMessage";
}