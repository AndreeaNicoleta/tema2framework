#include "MyProgressDialog.h"



MyProgressDialog::MyProgressDialog(bool autoClose) :MyDialog(visible)
{
	this->autoClose = autoClose;
}

MyProgressDialog::MyProgressDialog(const MyProgressDialog &obj) : MyDialog(visible)
{
	autoClose = obj.autoClose;
}


MyProgressDialog::~MyProgressDialog()
{
	std::cout << "Destructor from MyProgressDialog class.";
}

int MyProgressDialog::sizeHint() {
	return 1;
}

string MyProgressDialog::getName()
{
	return "MyProgressDialog";
}