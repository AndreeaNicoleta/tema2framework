#pragma once
#include "MyDialog.h"
using namespace std;
class MyErrorMessage :
	public MyDialog
{
private:
	bool showOnlyOnce;

public:
	MyErrorMessage(const MyErrorMessage &obj);
	MyErrorMessage(bool showOnlyOnce);
	~MyErrorMessage();
	int sizeHint();
	string getName();
};

