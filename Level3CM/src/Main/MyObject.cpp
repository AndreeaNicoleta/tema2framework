#include "MyObject.h"



MyObject::MyObject(string name)
{
	this->name = name;
}

MyObject::MyObject(const MyObject &obj)
{
	name = obj.name;
}

MyObject::~MyObject()
{
	std::cout << "Destructor from MyObject class.";
}

MyObject& MyObject::operator=(const MyObject &obj) {
	if (this != &obj) {
		this->name = obj.name;
	}
	return *this;
}
string MyObject::getName()
{
	return "MyObject";
}