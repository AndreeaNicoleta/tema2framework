#pragma once
#include "MyDialog.h"

using namespace std;
class MyProgressDialog :
	public MyDialog
{
private:
	bool autoClose;

public:
	MyProgressDialog(const MyProgressDialog &obj);
	MyProgressDialog(bool autoClose);
	~MyProgressDialog();
	int sizeHint();
	string getName();
};

