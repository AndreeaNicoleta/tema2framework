#include<iostream>
#include "MyObject.h"
#include "MyProgressDialog.h"
#include "MyFileDialog.h"
#include "MyErrorMessage.h"
#include "MyProgressDialog.h"
#include <vector>
#include <string.h>
using namespace std;
void main(){

	vector <MyObject*> myObjects(6);
	myObjects.push_back(new MyFileDialog(true));
	myObjects.push_back(new MyFileDialog(false));
	myObjects.push_back(new MyErrorMessage(true));
	myObjects.push_back(new MyErrorMessage(false));
	myObjects.push_back(new MyProgressDialog(true));
	myObjects.push_back(new MyProgressDialog(false));
	for (MyObject* obj : myObjects)
	{
		string name = obj->getName();
		cout << name << endl;
		int nr = obj->sizeHint();
		cout << nr << endl;


	}
}