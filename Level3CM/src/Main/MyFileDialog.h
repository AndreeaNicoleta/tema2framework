#pragma once
#include "MyDialog.h"
using namespace std;
class MyFileDialog :
	public MyDialog
{
private:
	bool preview;

public:
	MyFileDialog(const MyFileDialog &obj);
	MyFileDialog(bool preview);
	~MyFileDialog();
	string getName();
	int sizeHint();
};

