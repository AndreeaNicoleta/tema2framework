#pragma once
#include<string.h>
#include<iostream>
using namespace std;

class MyObject
{
protected:
	string name;

public:
	MyObject & operator =(const MyObject &obj);
	MyObject(const MyObject &obj);
	MyObject(string name);
	virtual ~MyObject();
	string getName();
};

