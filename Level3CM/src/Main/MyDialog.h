#pragma once
#include "MyWidget.h"
using namespace std;
class MyDialog :
	public MyWidget
{
protected:
	bool visible;
	virtual int sizeHint() = 0;
public:
	MyDialog(const MyDialog &obj);
	MyDialog(bool visible);
	~MyDialog();
	string getName();
};

